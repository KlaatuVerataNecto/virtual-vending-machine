Virtual Vending Machine

1. Run API 
cd api
dotnet run 

2. Set API url
web/components/VendingMachine.vue line 68

3. run web 
npm run serve

You can visit published version here:

https://virtual-vending-machine.azurewebsites.net/
