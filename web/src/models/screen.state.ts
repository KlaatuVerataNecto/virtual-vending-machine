export enum ScreenState {
    None,
    ProductSelected,
    PurchaseComplete
}