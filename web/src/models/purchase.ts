export interface IPurchase{
    productId: string;
    insertedCoins: number[];
}