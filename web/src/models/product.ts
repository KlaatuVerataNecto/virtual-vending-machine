export interface IProduct{
    id: string;
    name: string;
    priceInCents: number;
    quantityAvailable: number;
    imageUrl: string;
}