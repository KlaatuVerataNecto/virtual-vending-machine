using Microsoft.Extensions.Caching.Memory;
using NUnit.Framework;
using System;
using VendingMachine.Api.Core;
using VendingMachine.Api.Handlers.Commands;
using VendingMachine.Api.Store;

namespace VendingMachine.Tests
{
    public class PurchaseProductHandlerTests
    {
        // TODO: Here instead I would use in memory dbcontext
        private static PurchaseProduct.Handler GetHandler()
        {
            return new PurchaseProduct.Handler(new FakeStorageService(new MemoryCache(new MemoryCacheOptions())));
        }

        [Test]
        public void PurchaseProduct_EmptyInsertedCoins_ResturnsError()
        {
            // TODO: so here I would have a class like ProductBuilder which would use builder pattern to create in memory represenation of the Products
            // var product = new ProductBuilder(dc)
            //                      .WithId(Constants.AnyProductId)
            //                      .WithName(Constants.AnyString1)
            //                      .Build();
            // same for wallet

            var command = new PurchaseProduct.Command
            {
                ProductId = Guid.NewGuid(),
                InsertedCoins = Array.Empty<Coins>()
            };

            var result = GetHandler().Handle(command);
            Assert.IsNull(result);
        }
    }
}