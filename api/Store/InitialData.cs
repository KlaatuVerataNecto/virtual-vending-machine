﻿using System;
using System.Collections.Generic;
using VendingMachine.Api.Core;
using VendingMachine.Api.Models;

namespace VendingMachine.Api.Store
{
    public static class InitialData
    {
        public static Product[] Products = new[]
        {
            new Product {
                Id = Guid.NewGuid(),
                Name = "Tea",
                PriceInCents =  130,
                QuantityAvailable = 10,
                ImageUrl = "https://picsum.photos/id/225/100/100",
             },
            new Product {
                 Id = Guid.NewGuid(),
                Name = "Espresso",
                PriceInCents =  180,
                QuantityAvailable = 20,
                ImageUrl = "https://picsum.photos/id/113/100/100",
             },
            new Product {
                Id = Guid.NewGuid(),
                Name = "Juice",
                PriceInCents =  180,
                QuantityAvailable = 20,
                ImageUrl = "https://picsum.photos/id/30/100/100",
             },
            new Product {
                Id = Guid.NewGuid(),
                Name = "Chicken Soup",
                PriceInCents =  180,
                QuantityAvailable = 15,
                ImageUrl = "https://picsum.photos/id/326/100/100",
             }
        };

        public static IDictionary<Coins, int> Wallet = new Dictionary<Coins, int>()
        {
            { Coins.TenCents, 100 },
            { Coins.TwentyCents, 100 },
            { Coins.FiftyCents, 100 },
            { Coins.Euro, 100 },
        };
    }
}