using Microsoft.Extensions.Caching.Memory;

namespace VendingMachine.Api.Store
{
    public interface IFakeStorageService
    {
        T Get<T>(string key) where T : class;

        void Set<T>(string key, T value) where T : class;
    }

    public class FakeStorageService : IFakeStorageService
    {
        private IMemoryCache _cache;

        public FakeStorageService(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }

        public T Get<T>(string key) where T : class
        {
            return _cache.Get<T>(key);
        }

        public void Set<T>(string key, T value) where T : class
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetPriority(CacheItemPriority.NeverRemove);                    
            _cache.Set(key, value, cacheEntryOptions);
        }
    }
}