namespace VendingMachine.Api.Core
{
    public enum Coins
    {
        TenCents = 10,
        TwentyCents = 20,
        FiftyCents = 50,
        Euro = 100,
    }
}