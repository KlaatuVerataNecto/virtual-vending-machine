using System;
using VendingMachine.Api.Core;

namespace VendingMachine.Api.Models
{
    // TODO: that's here because most likely there would be another handler would use it (for example to a technician managing it)
    public class Wallet
    {
        public Coins Coin { get; set; }
        public int Quantity { get; set; }
    }
}