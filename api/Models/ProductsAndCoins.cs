using System;
using System.Collections.Generic;
using VendingMachine.Api.Core;

namespace VendingMachine.Api.Models
{
    public class ProductsAndCoins
    {
        public IReadOnlyCollection<Product> Products { get; set;} = Array.Empty<Product>();
        public IReadOnlyCollection<Coins> AvailableCoins { get; set;} = Array.Empty<Coins>();
    }
}