using System;

namespace VendingMachine.Api.Models
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int PriceInCents { get; set; }
        public string ImageUrl { get; set; }
        public int QuantityAvailable { get; set; }
    }
}