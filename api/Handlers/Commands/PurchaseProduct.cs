using System;
using System.Linq;
using System.Collections.Generic;
using VendingMachine.Api.Models;
using VendingMachine.Api.Store;
using VendingMachine.Api.Core;
using VendingMachine.Api.Extensions;

namespace VendingMachine.Api.Handlers.Commands
{
    public static class PurchaseProduct
    {
        // TODO: I would use Fluent Validator to automatically validate the command
        public sealed class Command
        {
            public Guid ProductId { get; set; }
            public Coins[] InsertedCoins { get; set; } = Array.Empty<Coins>();
        }

        public sealed class Response
        {
            public IReadOnlyCollection<Product> Products { get; set; } = Array.Empty<Product>();
            public IReadOnlyCollection<Coins> ReturnedCoins { get; set; } = Array.Empty<Coins>();
            public IReadOnlyCollection<Wallet> Wallet { get; set; } = Array.Empty<Wallet>();
        }

        public sealed class Handler
        {
            // TODO: in the real scenario we would use a DBContext, Redis or whatever store was planned to use
            public readonly IFakeStorageService _fakeStorageService;

            private static string ProductsKey = "products";
            private static string WalletKey = "wallet";

            public Handler(IFakeStorageService fakeStorageService)
            {
                _fakeStorageService = fakeStorageService;
            }

            public Response Handle(Command command)
            {
                // TODO: Product would be an entity and it would have a corresponding model
                // TODO: Entity would never leave the handler
                var products = _fakeStorageService.Get<Product[]>(ProductsKey);
                var wallet = _fakeStorageService.Get<Dictionary<Coins, int>>(WalletKey);

                // TODO: that would be always an empty List / Array, this check would-t be necessery
                if (products is null)
                {
                    return null;
                }

                var product = products.SingleOrDefault(x => x.Id == command.ProductId);
                if (product is null)
                {
                    // TODO: normally I would return and object better representing the response
                    // IsSuccess, IsFailure and Payload property if response is expected, with error message
                    // In this case I would provoke a failure
                    return null;
                }

                if (product.QuantityAvailable == 0)
                {
                    // TODO: same here
                    return null;
                }
                // Use greedy algorithm to calculate change
                var totalAmountInCents = command.InsertedCoins.Sum(x => (int)x);

                // Ooops maybe someone changed price of the product in the middle of user purchase
                if (product.PriceInCents > totalAmountInCents)
                {
                    return null;
                }

                // Subtract quantity, we know the machine can only sell one product at the time
                product.QuantityAvailable -= 1;

                foreach (var coin in command.InsertedCoins)
                {
                    wallet[coin] += 1;
                }

                var countOfCoinsToReturn = new Dictionary<Coins, int>();

                if (product.PriceInCents < totalAmountInCents)
                {
                    var amountInCentsToReturn = totalAmountInCents - product.PriceInCents;
                    CalculateChange(amountInCentsToReturn, Coins.Euro, countOfCoinsToReturn);

                    foreach (var coin in countOfCoinsToReturn)
                    {
                        wallet[coin.Key] -= coin.Value;
                        if (wallet[coin.Key] < 0)
                        {
                            // TODO: sorry, I'm out of change
                            return null;
                        }
                    }
                }

                var coinsToReturn = new List<Coins>();

                foreach (var coin in countOfCoinsToReturn)
                {
                    for (var i = 0; i < coin.Value; i++)
                    {
                        coinsToReturn.Add(coin.Key);
                    }
                }
                // TODO: do not return entity but model
                return new Response
                {
                    Products = products,
                    ReturnedCoins = coinsToReturn,
                    Wallet = wallet.Select(x => new Wallet { Coin = x.Key, Quantity = x.Value }).ToList()
                };
            }

            // Let's be greedy (TODO: I would never put this kind of comment)
            private static Dictionary<Coins, int> CalculateChange(double amount, Coins coin, Dictionary<Coins, int> coins)
            {
                var coinValue = (int)coin;
                if ((amount % coinValue) < amount)
                {
                    coins[coin] = (int)(amount / coinValue);
                    amount %= coinValue;
                }

                if (amount == 0)
                {
                    return coins;
                }

                return CalculateChange(amount, coin.Previous(), coins);
            }
        }
    }
}