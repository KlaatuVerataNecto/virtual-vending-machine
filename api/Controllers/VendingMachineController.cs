﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using VendingMachine.Api.Models;
using VendingMachine.Api.Core;
using VendingMachine.Api.Handlers.Commands;
using VendingMachine.Api.Store;

namespace VendingMachine.Api.Controllers
{
    [ApiController]
    [Route("vending-machine")]
    public class VendingMachineController : ControllerBase
    {
        // TODO: That would not be injected here
        public readonly IFakeStorageService _fakeStorageService;

        private static readonly string ProductsKey = "products";
        private static readonly string WalletKey = "wallet";

        public VendingMachineController(IFakeStorageService fakeStorageService)
        {
            _fakeStorageService = fakeStorageService;
            var products = _fakeStorageService.Get<Product[]>(ProductsKey);
            var wallet = _fakeStorageService.Get<Dictionary<Coins, int>>(WalletKey);

            if (products is null || wallet is null)
            {
                _fakeStorageService.Set(ProductsKey, InitialData.Products);
                _fakeStorageService.Set(WalletKey, InitialData.Wallet);
            }
        }

        [HttpGet]
        public ProductsAndCoins Get()
        {
            return new ProductsAndCoins
            {
                // TODO: I would use query handler + mediator
                Products = _fakeStorageService.Get<Product[]>(ProductsKey),
                AvailableCoins = Enum.GetValues(typeof(Coins)).Cast<Coins>().ToList()
            };
        }

        // TODO: this would literary be single line of code like this:
        // public async Task<IActionResult> Post(PurchaseProduct.Command command) => await CallHandlerAsync(command).ConfigureAwait(false);
        [HttpPost]
        public ActionResult Post(PurchaseProduct.Command command)
        {
            // TODO: This is done manually but I would use mediator here
            var handler = new PurchaseProduct.Handler(_fakeStorageService);
            var response = handler.Handle(command);
            if (response is null)
            {
                throw new ArgumentNullException(nameof(response));
            }
            return Ok(response);
        }
    }
}